```py

print("Hello world")
print('Hello world')
print('''You can include "" quotes here''' )
print("""This
is about
multiple lines 
of text 
already!""")

print('I\'m 14 years old')
print("Hello this is \"A\" letter")
print("apple\pen")
banana = "Banana" 
print("Apple", 'Orange', banana, sep="+")
print('Did you see that?' )

print(1,2,3,4,5, sep = "-", end="+")
print(10)
myint= 13
print(myint)
myint= 9+3
print(myint)


myAge = 25
print("My Age is:", myAge)
print("Im am %d yaers old" % myAge)
print('This is %d and this is %d' % (5, 4))

Paul = 9
print("%d%d%d%d" % (Paul, Paul, Paul, Paul))
print("%d  %d" % (Paul, Paul))
print("%d  %d" % (Paul, Paul))
print("%d%d%d%d" % (Paul, Paul, Paul, Paul))
print("Hello world")
print('Hello world')
print('''You can include "" quotes here''' )
print("""This
is about
multiple lines 
of text 
already!""")

print('I\'m 14 years old')
print("Hello this is \"A\" letter")
print("apple\pen")
banana = "Banana" 
print("Apple", 'Orange', banana, sep="+")
print('Did you see that?' )

print(1,2,3,4,5, sep = "-", end="+")
print(10)
myint= 13
print(myint)
myint= 9+3
print(myint)


myAge = 25
print("My Age is:", myAge)
print("Im am %d yaers old" % myAge)
print('This is %d and this is %d' % (5, 4))

Paul = 9
print("%d%d%d%d" % (Paul, Paul, Paul, Paul))
print("%d  %d" % (Paul, Paul))
print("%d  %d" % (Paul, Paul))
print("%d%d%d%d" % (Paul, Paul, Paul, Paul))

a = int(input())

print("   %d" % a)
print("  %d%d%d" % (a,a,a))
print(" %d%d%d%d%d" % (a,a,a,a,a))
print("%d%d%d%d%d%d%d" % (a,a,a,a,a,a,a))

p = float(input())

print("Donald Trump is %.2f kilos, yeah!" % p)
```